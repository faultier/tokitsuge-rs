//! # Tokitsuge
//!
//! A unit test friendly utility that provides the function to get current time.
//!
//! ## Feature flags
//!
//! - `freeze` provides functions to freeze the current time for unit testing.
//!
//! ## Usage
//!
//! Just replace `SystemTime::now` with `Clock::now`.
//!
//! ```no_run
//! use std::time::SystemTime;
//! use tokitsuge::Clock;
//!
//! fn some_time_depended_function() -> SystemTime {
//!     Clock::now()
//! }
//! ```
//!
//! This code itself just calls `SystemTime::now` to get the system time
//! (so there is no additional overhead in production).
//! Running the tests with the `freeze` feature enabled in `Cargo.toml` will change the behavior.
//!
//! ```no_run
//! use std::time::SystemTime;
//! use tokitsuge::Clock;
//!
//! fn some_time_depended_function() -> SystemTime {
//!     Clock::now()
//! }
//!
//! #[cfg(test)]
//! mod tests {
//!     use std::thread::sleep;
//!     use std::time::Duration;
//!     use super::*;
//!
//!     #[test]
//!     fn test_some_time_depended_function() {
//!         // Real system time.
//!         let t1 = some_time_depended_function();
//!
//!         {
//!             let frozen_clock = Clock::freeze();
//!
//!             // Fixed time.
//!             let ft1 = some_time_depended_function();
//!             assert_eq!(ft1, frozen_clock.fixed_time());
//!
//!             // This function will always return the same time until `frozen_clock` is dropped.
//!             sleep(Duration::from_millis(1));
//!             let ft2 = some_time_depended_function();
//!             assert_eq!(ft1, ft2);
//!
//!             // Instead of sleep, FrozenClock#advance and FrozenClock#unwind can be used.
//!             frozen_clock.advance(Duration::from_millis(1));
//!             let ft3 = some_time_depended_function();
//!             assert_eq!(ft2 < ft3);
//!         }
//!
//!         // Time flows again.
//!         let t2 = some_time_depended_function();
//!         assert!(t1 < t2);
//!     }
//! }
//! # fn main() {}
//! ```
//!
//! ## Note
//!
//! This utility **IS NOT** suitable for testing multithreaded operations.
//!
//! The test code and the code under test must run in the same thread,
//! as this utility uses thread-local variables
//! to prevent freeze effects from spreading to other tests.
//!
//! ## License
//!
//! Licensed under either of Apache License, Version 2.0 or MIT license at your option.

use std::time::SystemTime;

#[cfg(feature = "freeze")]
#[doc(inline)]
pub use freeze::FrozenClock;

/// Utility to get the current time.
///
/// If the `freeze` feature is not enabled,
/// only the [`now`] function (an alias of [`SystemTime::now`]) is provided.
///
/// If the `freeze` feature is enabled,
/// several functions are provided to freeze the current time for unit testing.
///
/// [`now`]: Clock::now
pub struct Clock;

#[cfg(not(feature = "freeze"))]
mod no_freeze {
    use super::*;

    impl Clock {
        /// Returns the system time corresponding to "now".
        ///
        /// This function is alias of [`SystemTime::now`].
        ///
        /// # Examples
        ///
        /// ```rust
        /// use tokitsuge::Clock;
        ///
        /// let sys_time = Clock::now();
        /// ```
        #[inline(always)]
        pub fn now() -> SystemTime {
            SystemTime::now()
        }
    }

    #[cfg(test)]
    mod tests {
        use std::thread::sleep;
        use std::time::Duration;

        use super::*;

        #[test]
        fn test_now() {
            let t1 = Clock::now();
            sleep(Duration::from_millis(1));
            let t2 = Clock::now();
            assert!(t1 < t2);
        }
    }
}

#[cfg(feature = "freeze")]
mod freeze {
    use std::cell::Cell;
    use std::fmt;
    use std::rc::Rc;
    use std::time::{Duration, UNIX_EPOCH};

    use super::*;

    thread_local! {
        static FIXED_TIME_KEY: Rc<Cell<Option<SystemTime>>> = Rc::new(Cell::new(None));
    }

    /// Duration since epoch at 2023-04-01T10:00:00 JST.
    ///
    /// This date has no special meaning in the system, but it is important to me.
    const DEFAULT_DURATION_SINCE_EPOCH: Duration = Duration::from_secs(1680310800);

    /// RAII implementation of Clock's scoped freeze.
    ///
    /// When this structure is dropped (out of scope),
    /// the clock freeze in the current thread is unfrozen.
    ///
    /// The fixed time can be advanced or reversed through this guard.
    pub struct FrozenClock {
        fixed: Rc<Cell<Option<SystemTime>>>,
    }

    impl FrozenClock {
        /// Returns the fixed time in the current thread.
        ///
        /// # Examples
        ///
        /// ```rust
        /// use std::time::UNIX_EPOCH;
        /// use tokitsuge::Clock;
        ///
        /// let frozen_clock = Clock::freeze_at(UNIX_EPOCH);
        /// assert_eq!(frozen_clock.fixed_time(), UNIX_EPOCH);
        /// ```
        pub fn fixed_time(&self) -> SystemTime {
            self.fixed.get().unwrap()
        }

        /// Advance the clock in the current thread by `duration`.
        ///
        /// # Examples
        ///
        /// ```rust
        /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
        /// use std::time::Duration;
        /// use tokitsuge::Clock;
        ///
        /// let frozen_clock = Clock::freeze();
        /// let t1 = Clock::now();
        /// frozen_clock.advance(Duration::from_millis(1));
        /// let t2 = Clock::now();
        ///
        /// assert!(t1 < t2);
        /// assert_eq!(t2.duration_since(t1)?.as_millis(), 1);
        /// # Ok(())
        /// # }
        /// ```
        ///
        /// # Panics
        ///
        /// This function may panic if the `self.fixed_time() + duration` cannot be
        /// represented by [`SystemTime`].
        pub fn advance(&self, duration: Duration) {
            self.set(self.fixed_time() + duration)
        }

        /// Unwind the clock in the current thread by `duration`.
        ///
        /// # Examples
        ///
        /// ```rust
        /// # fn main() -> Result<(), Box<dyn std::error::Error>> {
        /// use std::time::Duration;
        /// use tokitsuge::Clock;
        ///
        /// let frozen_clock = Clock::freeze();
        /// let t1 = Clock::now();
        /// frozen_clock.unwind(Duration::from_millis(1));
        /// let t2 = Clock::now();
        ///
        /// assert!(t1 > t2);
        /// assert_eq!(t1.duration_since(t2)?.as_millis(), 1);
        /// # Ok(())
        /// # }
        /// ```
        ///
        /// # Panics
        ///
        /// This function may panic if the `self.fixed_time() - duration` cannot be
        /// represented by [`SystemTime`].
        pub fn unwind(&self, duration: Duration) {
            self.set(self.fixed_time() - duration)
        }

        /// Set the clock in the current to time `t`.
        ///
        /// # Examples
        ///
        /// ```rust
        /// use std::time::UNIX_EPOCH;
        /// use tokitsuge::Clock;
        ///
        /// let frozen_clock = Clock::freeze();
        /// assert_ne!(Clock::now(), UNIX_EPOCH);
        ///
        /// frozen_clock.set(UNIX_EPOCH);
        /// assert_eq!(Clock::now(), UNIX_EPOCH);
        /// ```
        pub fn set(&self, t: SystemTime) {
            self.fixed.set(Some(t))
        }
    }

    impl fmt::Debug for FrozenClock {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            f.debug_tuple("FrozenClock")
                .field(&self.fixed_time())
                .finish()
        }
    }

    impl Drop for FrozenClock {
        fn drop(&mut self) {
            self.fixed.set(None)
        }
    }

    impl Clock {
        /// Returns the fixed time if [`Clock::freeze`] or [`Clock::freeze_at`] was called
        /// before this function in the current thread,
        /// otherwise the system time corresponding to ["now"].
        ///
        /// # Examples
        ///
        /// ```rust
        /// use tokitsuge::Clock;
        ///
        /// let sys_time = Clock::now();
        /// ```
        ///
        /// ["now"]: SystemTime::now
        pub fn now() -> SystemTime {
            FIXED_TIME_KEY.with(|cell| cell.get().unwrap_or_else(SystemTime::now))
        }

        /// Returns whether the clock is frozen in the current thread.
        ///
        /// # Examples
        ///
        /// ```rust
        /// use tokitsuge::Clock;
        ///
        /// assert!(!Clock::is_frozen());
        ///
        /// {
        ///     let _guard = Clock::freeze();
        ///     assert!(Clock::is_frozen());
        /// }
        ///
        /// assert!(!Clock::is_frozen());
        /// ```
        pub fn is_frozen() -> bool {
            FIXED_TIME_KEY.with(|cell| cell.get().is_some())
        }

        /// Freezes the current thread clock at the time `2023-04-01T10:00:00 JST`.
        ///
        /// Until the return value [`FrozenClock`] is dropped,
        /// [`Clock::now`] always returns the fixed time.
        ///
        /// # Examples
        ///
        /// ```rust
        /// use std::thread::sleep;
        /// use std::time::Duration;
        /// use tokitsuge::Clock;
        ///
        /// let _guard = Clock::freeze();
        /// let t1 = Clock::now();
        /// sleep(Duration::from_millis(1));
        /// let t2 = Clock::now();
        /// assert_eq!(t1, t2);
        /// ```
        pub fn freeze() -> FrozenClock {
            Self::freeze_at(UNIX_EPOCH + DEFAULT_DURATION_SINCE_EPOCH)
        }

        /// Freezes the current thread clock at `time`.
        ///
        /// Until the return value [`FrozenClock`] is dropped,
        /// [`Clock::now`] always returns the fixed time.
        ///
        /// # Examples
        ///
        /// ```rust
        /// use std::time::UNIX_EPOCH;
        /// use tokitsuge::Clock;
        ///
        /// let _guard = Clock::freeze_at(UNIX_EPOCH);
        /// assert_eq!(Clock::now(), UNIX_EPOCH);
        /// ```
        pub fn freeze_at(time: SystemTime) -> FrozenClock {
            FIXED_TIME_KEY.with(|cell| {
                assert!(cell.get().is_none(), "clock is already frozen");
                cell.set(Some(time));
                let fixed = cell.clone();
                FrozenClock { fixed }
            })
        }
    }

    #[cfg(test)]
    mod tests {
        use std::thread::sleep;
        use std::time::Duration;

        use super::*;

        #[test]
        fn test_system_time() {
            let t1 = Clock::now();
            sleep(Duration::from_millis(1));
            let t2 = Clock::now();
            assert!(t1 < t2);
        }

        #[test]
        fn test_freeze() {
            assert!(!Clock::is_frozen());

            {
                let frozen = Clock::freeze();
                let t0 = frozen.fixed_time();
                assert!(Clock::is_frozen());
                assert_eq!(format!("{:?}", frozen), format!("FrozenClock({:?})", t0));

                sleep(Duration::from_millis(1));
                let t1 = Clock::now();
                assert_eq!(t1, t0);

                frozen.advance(Duration::from_millis(1));
                let t2 = Clock::now();
                assert!(t2 > t1);

                frozen.unwind(Duration::from_millis(2));
                let t3 = Clock::now();
                assert!(t3 < t2);
            }

            assert!(!Clock::is_frozen());
        }

        #[test]
        #[should_panic]
        fn test_multiple_freeze() {
            let _frozen1 = Clock::freeze();
            let _frozen2 = Clock::freeze();
        }
    }
}
